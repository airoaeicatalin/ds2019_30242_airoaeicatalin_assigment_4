package com.world.world.mapper;

import com.world.world.dto.NameIdDTO;
import com.world.world.entity.Network;
import com.world.world.entity.Post;
import com.world.world.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface NameIdMapper {

    @Mapping(target = "name", source = "username")
    NameIdDTO userToNameIdDTO(User user);

    @Mapping(target = "name", source = "title")
    NameIdDTO postToNameIdDTO(Post post);

    NameIdDTO networkToNameIdDTO(Network network);
}
