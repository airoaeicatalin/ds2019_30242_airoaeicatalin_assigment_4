package com.world.world.mapper;

import com.world.world.dto.NameIdDTO;
import com.world.world.dto.UserDTO;
import com.world.world.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

@Mapper(uses = NameIdMapper.class)
public abstract class UserMapper {

    @Autowired
    EntityManager entityManager;

    @Mapping(target = "username", source = "name")
    public abstract User toEntity(UserDTO userDTO);

    @Mapping(target = "username", source = "name")
    public abstract void toEntityUpdate(@MappingTarget User user, UserDTO userDTO);

    @Mapping(target = "name", source = "username")
    public abstract UserDTO toDTO(User user);

    public User toEntity(NameIdDTO nameIdDTO) {
        return entityManager.find(User.class, nameIdDTO.getId());
    }
}
