package com.world.world.mapper;

import com.world.world.dto.PostDTO;
import com.world.world.entity.Post;
import com.world.world.entity.User;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;

@Mapper(uses = NameIdMapper.class)
public abstract class PostMapper {

    @Autowired
    EntityManager entityManager;

    @Mapping(target = "title", source = "name")
    @Mapping(target = "networkId", source = "network.id")
    @Mapping(target = "author", ignore = true)
    public abstract Post toEntity(PostDTO postDTO);

    @Mapping(target = "title", source = "name")
    @Mapping(target = "networkId", source = "network.id")
    @Mapping(target = "author", ignore = true)
    public abstract void toEntityUpdate(@MappingTarget Post post, PostDTO postDTO);

    @Mapping(target = "name", source = "title")
    public abstract PostDTO toDTO(Post post);

    @AfterMapping
    void toDtoAfterMapping(@MappingTarget Post post, PostDTO postDTO) {
        final User author = entityManager.find(User.class, postDTO.getAuthor().getId());
        post.setAuthor(author);
    }
}
