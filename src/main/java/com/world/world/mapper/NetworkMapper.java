package com.world.world.mapper;

import com.world.world.dto.NetworkDTO;
import com.world.world.entity.Network;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(uses = NameIdMapper.class)
public interface NetworkMapper {

    @Mapping(target = "users", ignore = true)
    @Mapping(target = "posts", ignore = true)
    Network toEntity(NetworkDTO networkDTO);

    @Mapping(target = "users", ignore = true)
    @Mapping(target = "posts", ignore = true)
    void toEntityUpdate(@MappingTarget Network network, NetworkDTO networkDTO);

    NetworkDTO toDTO(Network network);
}
