package com.world.world.dto;

import com.world.world.enumeration.UserRole;

public class UserDTO extends NameIdDTO  {

    private String username;
    private String password;
    private UserRole role;

    @Override
    public String getName() {
        return username;
    }

    @Override
    public void setName(String name) {
        this.username = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
