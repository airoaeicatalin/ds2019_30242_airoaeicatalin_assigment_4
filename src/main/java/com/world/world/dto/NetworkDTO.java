package com.world.world.dto;

import java.util.List;

public class NetworkDTO extends NameIdDTO  {

    private List<NameIdDTO> users;
    private List<NameIdDTO> posts;

    public List<NameIdDTO> getUsers() {
        return users;
    }

    public void setUsers(List<NameIdDTO> users) {
        this.users = users;
    }

    public List<NameIdDTO> getPosts() {
        return posts;
    }

    public void setPosts(List<NameIdDTO> posts) {
        this.posts = posts;
    }
}
