package com.world.world.dto;

public class PostDTO extends NameIdDTO {

    private String title;
    private String content;
    private NameIdDTO author;
    private NameIdDTO network;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public NameIdDTO getAuthor() {
        return author;
    }

    public void setAuthor(NameIdDTO author) {
        this.author = author;
    }

    public NameIdDTO getNetwork() {
        return network;
    }

    public void setNetwork(NameIdDTO network) {
        this.network = network;
    }
}
