package com.world.world.repository;

import com.world.world.entity.Network;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface NetworkRepository extends JpaRepository<Network, Long> {

    Optional<Network> findById(final Long id);
}
