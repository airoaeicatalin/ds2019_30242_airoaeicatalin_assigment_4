package com.world.world.entity;

import javax.persistence.*;
import java.util.List;

@Entity(name = "ps_network")
public class Network {

    @Id
    @GeneratedValue
    private Long id;
    private String name;

    @ManyToMany
    private List<User> users;

    @OneToMany
    private List<Post> posts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Post> getPosts() {
        return posts;
    }

    public void setPosts(List<Post> posts) {
        this.posts = posts;
    }
}
