package com.world.world.entity;

import javax.persistence.*;

@Entity(name = "ps_post")
public class Post {

    @Id
    @GeneratedValue
    private Long id;
    private String title;
    private String content;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @Column(name = "network_id", nullable = false)
    private Long networkId;

    @ManyToOne
    @JoinColumn(name = "network_id", insertable = false, updatable = false)
    private Network network;

    // here you can see two examples of setting a foreign object in a manyToOne relationship
    // you can make another attribute with the id alongside the object
    // (networkId and network both use in the DB the same field: network_id)
    // the author will only be changed by setting a author object
    // the network will be changed by setting only the id (not the whole object)
    // in both cases, the whole object that is linked will be fetched
    // see the way they are setted in the mapper

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Network getNetwork() {
        return network;
    }

    public Long getNetworkId() {
        return networkId;
    }

    public void setNetworkId(Long networkId) {
        this.networkId = networkId;
    }
}
