package com.world.world.controller;

import com.world.world.dto.UserDTO;
import com.world.world.entity.User;
import com.world.world.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserRestController {

    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/test")
    public String test() {
        return userService.test();
    }

    @GetMapping("/list")
    public List<UserDTO> list() {
        return userService.findAll();
    }
}
