package com.world.world.controller;

import com.world.world.dto.NetworkDTO;
import com.world.world.entity.User;
import com.world.world.service.NetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/network")
public class NetworkRestController {

    private final NetworkService networkService;

    @Autowired
    public NetworkRestController(NetworkService networkService) {
        this.networkService = networkService;
    }

    @GetMapping("/list")
    public List<NetworkDTO> list() {
        return networkService.findAll();
    }

    @GetMapping(path = "/{id}")
    public NetworkDTO find(@PathVariable("id") Long id)  {
        return networkService.findById(id);
    }
}
