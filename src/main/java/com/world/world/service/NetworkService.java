package com.world.world.service;

import com.world.world.dto.NetworkDTO;

import java.util.List;

public interface NetworkService {

    NetworkDTO findById(Long id);

    List<NetworkDTO> findAll();

    Long save(NetworkDTO networkDTO);
}
