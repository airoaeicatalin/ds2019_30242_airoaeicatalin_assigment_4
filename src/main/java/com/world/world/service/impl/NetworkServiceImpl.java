package com.world.world.service.impl;

import com.world.world.dto.NetworkDTO;
import com.world.world.entity.Network;
import com.world.world.mapper.NetworkMapper;
import com.world.world.repository.NetworkRepository;
import com.world.world.service.NetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NetworkServiceImpl implements NetworkService {

    private final NetworkMapper networkMapper;
    private final NetworkRepository networkRepository;

    @Autowired
    public NetworkServiceImpl(NetworkMapper networkMapper,
                              NetworkRepository networkRepository) {
        this.networkMapper = networkMapper;
        this.networkRepository = networkRepository;
    }

    @Override
    public NetworkDTO findById(Long id) {
        final Network network = networkRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Cannot find network with ID: " + id));
        return networkMapper.toDTO(network);
    }

    @Override
    public List<NetworkDTO> findAll() {
        return networkRepository.findAll()
                .stream()
                .map(networkMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public Long save(NetworkDTO networkDTO) {
        final Network network = networkDTO.getId() != null ?
                networkRepository.findById(networkDTO.getId()).orElseThrow(EntityNotFoundException::new) : new Network();


        networkMapper.toEntityUpdate(network, networkDTO);
        return networkMapper.toDTO(networkRepository.save(network)).getId();
    }
}
