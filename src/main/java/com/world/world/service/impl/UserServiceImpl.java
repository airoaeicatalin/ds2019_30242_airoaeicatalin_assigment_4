package com.world.world.service.impl;

import com.world.world.dto.UserDTO;
import com.world.world.entity.User;
import com.world.world.mapper.UserMapper;
import com.world.world.repository.UserRepository;
import com.world.world.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserMapper userMapper,
                           UserRepository userRepository) {
        this.userMapper = userMapper;
        this.userRepository = userRepository;
    }

    @Override
    public String test() {
        return "text din user service";
    }

    @Override
    public UserDTO save(UserDTO userDto) {

        User user;
        if(userDto.getId() != null) {
            user = userRepository.findById(userDto.getId());
        } else {
            user = new User();
        }

        userMapper.toEntityUpdate(user, userDto);
        return userMapper.toDTO(userRepository.save(user));
    }

    @Override
    public List<UserDTO> findAll() {

        return userRepository.findAll()
                .stream()
                .map(userMapper::toDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO findById(Long id) {
        return userMapper.toDTO(userRepository.findById(id));
    }
}
