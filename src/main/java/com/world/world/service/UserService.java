package com.world.world.service;

import com.world.world.dto.UserDTO;
import com.world.world.entity.User;

import java.util.List;

public interface UserService {

    UserDTO save(UserDTO userDto);

    String test();

    List<UserDTO> findAll();

    UserDTO findById(Long id);
}
