package com.ps.common.dto;

public class OrderDTO {

    private Long id;
   // private int total;

    private PriceIdDTO pizza;
    private PriceIdDTO voucher;
    private PriceIdDTO drink;
    private NameIdDTO address;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PriceIdDTO getPizza() {
        return pizza;
    }

    public void setPizza(PriceIdDTO pizza) {
        this.pizza = pizza;
    }

    public PriceIdDTO getVoucher() {
        return voucher;
    }

    public void setVoucher(PriceIdDTO voucher) {
        this.voucher = voucher;
    }

    public PriceIdDTO getDrink() {
        return drink;
    }

    public void setDrink(PriceIdDTO drink) {
        this.drink = drink;
    }

    public NameIdDTO getAddress() {
        return address;
    }

    public void setAddress(NameIdDTO address) {
        this.address = address;
    }


}
