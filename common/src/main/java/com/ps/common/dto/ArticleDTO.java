package com.ps.common.dto;

import java.util.List;
//import com.ps.backend.entity.*;

public class ArticleDTO extends NameIdDTO{

    private NameIdDTO author;

    private NameIdDTO categ;

    private Long id;
    private String title;
    private String abstr;
    private String body;

    private List<NameIdDTO> related;

    public NameIdDTO getAuthor() {
        return author;
    }

    public void setAuthor(NameIdDTO author) {
        this.author = author;
    }

    public NameIdDTO getCateg() {
        return categ;
    }

    public void setCateg(NameIdDTO categ) {
        this.categ = categ;
    }


    public Long getId() {
        return id;
    }


    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAbstr() {
        return abstr;
    }

    public void setAbstr(String abstr) {
        this.abstr = abstr;
    }


    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public List<NameIdDTO> getRelated() {
        return related;
    }

    public void setRelated(List<NameIdDTO> related) {
        this.related = related;
    }


}
