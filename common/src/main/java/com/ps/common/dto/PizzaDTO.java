package com.ps.common.dto;

import java.util.List;

public class PizzaDTO {

   // private NameIdDTO author;

    private NameIdDTO categ;

    private Long id;
    private String name;
    private int quantity;
    private int price;

    private List<NameIdDTO> related;

    public NameIdDTO getCateg() {
        return categ;
    }

    public void setCateg(NameIdDTO categ) {
        this.categ = categ;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<NameIdDTO> getRelated() {
        return related;
    }

    public void setRelated(List<NameIdDTO> related) {
        this.related = related;
    }
}
